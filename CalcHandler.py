#!/usr/bin/env python

import pandas as pd
from pytexit import py2tex
import pylatex as plx
import re

class Property():
    "A class for property objects, to store incrementing and unit info"

    def __init__(self, name, allowed_values, units):
        """ Creates a Property object to be used in design calcs.

            'name' is a string name for the property. 'allowed_values' is an
            iterable containing all alowable values for the property, this
            could be a range or a list of specific values. 'units' is a string
            showing the units of the values given e.g. 'mm'.

            Constructor: Property(str, iterator, str)
        """
        self.name = name
        self.allowed_values = allowed_values
        self.units = units
        self.value = allowed_values[0]
        self.value_iterator = None

    def value_reset(self, new_val):
        """ Resets self.value to the next allowed value and resets the iterator
            to start from there.
        """
        if new_val:
            for val in self:
                if val >= new_val:
                    self.value = val
                    break
        else:
            self.value = self.allowed_values[0]

    def __iter__(self):
        """ Returns the iterator object of allowed_values """
        self.value_iterator = iter(self.allowed_values)
        return self

    def __next__(self):
        """ Returns the next value in allowed_values. """
        return next(self.value_iterator)

class CalcBase():
    "A base class to store generalised design functionality"
    # TODO: Change data access so property could either be set or changeable - read in to 'self'?
    METHOD, MULTICOMMENT, COMMENT, OTHER = 'mCco'

    def __init__(self, design_info, settings_file=None, **settings_override):
        """ Creates a CalcBase instance for a given calculation set.

            design_info is a file path of a spreadsheet containing design
            actions and object properties for each object to be designed. Note
            spreadsheet column names must match exactly with variable names
            used in the design calculations.

            Constructor: CalcBase(str)
        """
        self.data = self.extract_design_info(design_info)
        self.data_index = 2

    def design_loop(self, new_summary_file=True, verbose=False):
        """ Runs the iterative design process using the given calc_class and
            design instances.

            If new_summary_file is True, then design results are saved in a
            new spreadsheet. If it is False, they are appended to the existing
            design_info spreadsheet.
        """
        # TODO: add verbose mode

        # Define some data structure for putting the results in
        property_names = [prop.name for prop in self.properties.keys()]
        criteria_names = [crit.__name__ for crit in self.criteria.keys()]
        design_result = pd.DataFrame([prop.units for prop in 
                                      self.properties.keys()],
                                     columns=property_names)
        calc_result = pd.DataFrame(columns=criteria_names).append(pd.Series(), 
                                                            ignore_index=True)

        # iterate for row in data table
        for index, design_object in self.data.iloc[1:].iterrows():
            self.data_index = index
            #TODO: How will users navigate this and design actions?
            # need to set temporary instance variables?
            if verbose: print("starting_design for   " + design_object[0])
            # start at the minimum values, either code or practical limits
            for prop in self.properties:
                if self.properties[prop]: # min criteria method is not None
                    start_val = self.properties[prop](self) # call min criteria method
                else:
                    start_val = None
                prop.value_reset(start_val)
            # reset the incrementation failure flag to False
            self.increment_failed = False

            # check the design criteria, incrementing properties as needed
            self.design_step(verbose)
            # Do a final pass to check everything is still ok
            final_utilisations = self.check_object()

            # If everything is not ok, repeat the design process a few times,
            # unless there was an increment failure.
            if not self.increment_failed:
                num_attempts = 0
                while any in final_utilisations > 1 and num_attempts < 3:
                    self.design_step(verbose)
                    final_utilisations = self.check_object()
                    num_attempts += 1

            # Run any remaining checks not appropriate for incrementing
            check_only_results = []
            check_only_names = [check.__name__ for check in
                                self.check_only_methods]
            for check in self.check_only_methods:
                check_only_results.append(check(self))

            # Store the resulting properties and utilistations
            prop_vals = [prop.value for prop in self.properties]
            design_result = design_result.append(dict(zip(property_names,
                                                          prop_vals)),
                                                ignore_index=True)
            calc_result = calc_result.append(dict(zip(criteria_names+\
                                                      check_only_names,
                                                      final_utilisations+\
                                                      check_only_results)),
                                            ignore_index=True)

        # write results to summary file
        save_location = self.get_save_location(new_summary_file)
        self.export_summary(design_result, calc_result, save_location)

    def check_object(self):
        """ Check the current property values against the criteria methods.

            Returns the utilisation ratios for all criteria
        """
        util_ratios = []
        for crit_method in self.criteria:
            # check utilisation
            utilisation = crit_method(self)
            util_ratios.append(utilisation)

        return util_ratios

    def design_step(self, verbose=False):
        """ Perform one design iteration on the current property values.

            Returns the utilisation ratios for all criteria
        """
        util_ratios = []
        for crit_method in self.criteria:
            satisfied = False
            increment_count = 0
            while not satisfied:
                # check utilisation
                utilisation = crit_method(self)
                if utilisation < 1:
                    satisfied = True
                else:
                    increment_count += 1
                    incremented = self.increment(crit_method, increment_count)
                    if not incremented:
                        self.increment_failed = True
                        if verbose: print("Incrementation failed")
                        break
            util_ratios.append(utilisation)

        return util_ratios

    def increment(self, crit, increment_count):
        """ Increments the appropriate property based on criteria, as specified
            in the calc_class.

            Returns a success boolean of True for completed increment, or False
            for failure if the limit has been reached.
        """
        # decide what to increment
        prop_preference = self.criteria[crit]
        if increment_count % sum(prop_preference[1]) \
           in range(1,prop_preference[1][0]):
            prop = prop_preference[0][0]
        elif len(prop_preference[0]) > 1:
            prop = prop_preference[0][1]
        else:
            prop = prop_preference[0][0]

        # actually do the incrementing now
        try:
            prop.value = next(prop)
            return True
        except StopIteration:
            return False

    def extract_design_info(self, design_info):
        """ Reads the spreadsheet and puts into a python data structure """
        return pd.read_excel(design_info, true_values=['T','t'],
                             false_values=['F','f'])

    def export_summary(self, design_result, calc_result, save_location):
        """ Write the data to the save location """
        # Print the auto-generated save location
        print(pd.concat([self.data, design_result, calc_result], axis=1))

    def export_pdf(self):
        """ Interpret calc functions into pdf output

            Fill out pre-saved template with variable values?
            Execute interpretation every time?

            Fiddle with the python code to get something readable?
        """
        components, types = self.get_document_components()
        self.compile_document(components, types)

    def get_document_components(self):
        """ Returns a list of document components and a list of their types.

            Including: method names, docstrings, comments, and other code.

            Single line comments must be denoted with '#!' to be included.
        """
        method_names = [i.__name__ for i in self.criteria.keys()]
        #method_names = ['dummy']
        methods_done = []
        components = []
        component_types = []

        #txt_file = self.export_code_text()
        #with open(txt_file) as f:
        with open(__file__, 'r') as f:
            #docstring = False
            in_method = False
            for line in f:
                line_stripped = line.strip()
                method = next((method for method in method_names if
                               f"def {method}" in line), False)
                if method:
                    components.append(line_stripped)
                    component_types.append(self.METHOD)
                    methods_done.append(method)
                    method_names.remove(method)

                    docstring = False
                    in_method = True

                elif in_method and not line_stripped.startswith('def '):
                    # TODO: only check for docstring at start of method
                    if not docstring and (line_stripped.startswith('"""') or
                                          line_stripped.startswith("'''")):
                        docstring = True
                        docstring_marker = line_stripped[:3]
                        components.append('')
                    if docstring:
                        components[-1] += line
                        if line_stripped.endswith(docstring_marker):
                            docstring = False
                            component_types.append(self.MULTICOMMENT)
                        continue
                    elif line_stripped.startswith('#!'):
                        components.append(f"comment: {line_stripped[2:]}")
                        component_types.append(self.COMMENT)
                        continue
                    elif line_stripped.startswith('#'):
                        continue
                    elif 'return ' not in line_stripped:
                        components.append(line)
                        component_types.append(self.OTHER)

                else:
                    in_method = False

            if method_names:
                print(f"Method/(s) {method_names} not found in this file,"+\
                      "look in superclass")

            #for l in document:
                #print(l)

            return components, component_types

    def compile_document(self, components, types):
        """ Generates a pdf document containing formatted components.
        """
        # Initialise the document
        doc = plx.Document()

        doc.preamble.append(plx.Command('title', __class__.__name__))
        doc.preamble.append(plx.Command('author', 'Anonymous author'))
        doc.preamble.append(plx.Command('date', plx.utils.NoEscape(r'\today')))
        doc.append(plx.utils.NoEscape(r'\maketitle'))

        # Parse components
        sec = None
        continuation = "\n"
        variables = {}
        method_args = []
        for index, component in enumerate(components):
            if not component.strip():
                continue
            type_ = types[index]
            if type_ == self.METHOD:
                #TODO: parse method name
                if sec:
                    sec.__exit__(None, None, None)
                method_name = component[4:component.find('(')]
                method_args = self.get_function_variables(component)
                sec = doc.create(plx.Section(method_name))
                sec.__enter__()

            elif type_ in (self.COMMENT, self.MULTICOMMENT):
                #TODO: parse comments - deal with bad characters etc.
                doc.append(component + '\n')
                for var in method_args:
                    latex_var = self.convert_variable_name(var)
                    doc.append(plx.utils.NoEscape(latex_var+ \
                                                  ' = \\'+latex_var+'\\\\'))
                    variables[var] = latex_var
            else:
                #TODO: parse other, including assignments + equations.
                cleaned_comp = component[:component.find('#')]
                cleaned_comp = cleaned_comp.replace("self.", '')
                cleaned_comp = cleaned_comp.replace('.value', '')
                cleaned_comp = cleaned_comp.replace("'", '')
                cleaned_comp = cleaned_comp.replace('"', "")
                cleaned_comp = cleaned_comp.replace(' ', '')
                # cleaned_comp = cleaned_comp.replace('_', '')
                while 'get(' in cleaned_comp:
                    index = cleaned_comp.find('get(')
                    cleaned_comp = cleaned_comp.replace('get(', '')
                    open_brackets = 1
                    closed_brackets = 0
                    while True:
                        if cleaned_comp[index] == '(':
                            open_brackets += 1
                        elif cleaned_comp[index] == ')':
                            closed_brackets += 1
                            if closed_brackets == open_brackets:
                                cleaned_comp = cleaned_comp[:index] \
                                               + cleaned_comp[index+1:]
                                break
                        index += 1

                if cleaned_comp[-1] == "\\":
                    continuation += cleaned_comp[:-1]
                    continue
                line_to_append = continuation + cleaned_comp
                if '=' in line_to_append:
                    eq_index = line_to_append.find('=')
                    var = line_to_append[:eq_index].replace('\n','')
                    latex_var = self.convert_variable_name(var)
                    variables[var] = latex_var

                doc.append(plx.utils.NoEscape(py2tex(line_to_append)))
                if continuation:
                    continuation = "\n"

        if sec:
            sec.__exit__(None, None, None)

        # Add variables to document preamble
        for var in variables:
            doc.preamble.append(plx.utils.NoEscape('\\newcommand{\\'+ \
                                                   variables[var]+'}{0}'))
        # Generate output pdf
        doc.generate_pdf(__class__.__name__, clean_tex=False)
        print(variables)

    def get_function_variables(self, func_def):
        """ Returns a list of variables found in the function declaration line. """
        arg_region = func_def[func_def.find('(')+1:func_def.rfind(')')]
        pattern = re.compile('\([\w\s:,.=\[\]\{\}"' + "'" + ']*\)|'
                             '\{[\w\s:,.=\[\]\(\)"' + "'" + ']*\}|'
                             '\[[\w\s:,.=\{\}\(\)"' + "'" + ']*\]')
        while '(' in arg_region or '[' in arg_region or '{' in arg_region:
            arg_region = pattern.sub('',arg_region)
        args = [val.split('=')[0].strip() for val in arg_region.split(',')]
        if args[0] == 'self':
            args = args[1:]
        return args

    def convert_variable_name(self, name):
        """ Returns string that can be used as a latex variable """
        # TODO: Deal with possible indexing problems
        # TODO: Deal with numbers in variable names
        while '_' in name:
            i = name.find('_')
            name = name[:i] + name[i+1].upper() + name[i+2:]
        return name

    def export_code_text(self):
        """ Saves a text version of this python file and returns the name"""

        txt_file = __file__[:-2] + 'txt'
        txt = open(__file__, 'r').readlines()
        open(txt_file, 'w').writelines(txt)
        return txt_file

    def set_defaults(self):
        """ Set class variables from a file? """
        pass

    def get_save_location(self, new_summary_file):
        """ Returns the file path to use for saving.

            new_summary_file is a Boolean - True for saving to a new file,
            false for using the file parsed in as design_info.
        """
        # Could also be useful to ask user for a path
        return ''

    def default_method(self):
        """ Returns something.
        """
        something = self.get('ULS Axial Force') \
                    / (self.default_property.value * 100)
        return something

    def another_default_method(self, default_variable=1):
        # A comment that won't get into the output
        #! A comment I want you to see!
        another_thing = 3*default_variable
        return another_thing

    def default_min_criteria_method(self):
        """ Returns something else.
        """
        return 1

    def default_check_method(self):
        """ Returns something else again
        """
        return self.data_index % 2

    def get(self, column):
        """ Returns the value(s) from self.data in column at self.data_index

            Column must be the string column name as stored in self.data
        """
        # modify get so Property objects also use the method
        ## set Property objects and criteria in __init__ by str names, not
        # actual objects
        ## may need another dict mapping string names to property objects
        return self.data[column][self.data_index]

    default_property = Property('default property', [0, 1, 2, 3], '[mm]')
    properties = {default_property: default_min_criteria_method}
    criteria = {default_method: ((default_property,), (1,)),
                another_default_method: ()}
    check_only_methods = [default_check_method]


if __name__ == "__main__":
    testBase = CalcBase('ExampleData.xlsx')
    #testBase.design_loop()
    testBase.export_pdf()
